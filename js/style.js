"use strict";

const urlUsers=`https://ajax.test-danit.com/api/json/users`;
const urlPosts=`https://ajax.test-danit.com/api/json/posts`;

class Request{
    getPost(url){
        return fetch(url).then(response=>response.json());
    }
    deletePost(idPost){
        return fetch(`https://ajax.test-danit.com/api/json/posts/${idPost}`,{
            method:"DELETE",
        }).then(response=>response.status);
    }
}

class Card{
    constructor(){
        this.request=new Request();
    }
    getPosts(){
        const users=this.request;
        users.getPost(urlUsers).then(users=>{
            users.forEach(({id,name,email}=el) => {
                const IdUser=id;
                const posts=this.request;
                posts.getPost(urlPosts).then(posts=>{
                    posts.forEach(({id,title,body,userId}=e)=>{
                        if (IdUser===userId){
                            let post={};
                            post.id=id;
                            post.name=name;
                            post.email=email;
                            post.title=title;
                            post.body=body;
                            this.render(post);
                        }
                    })
                });
            });
        });
    }
    render({id,name,email,title,body}){
        const div=document.createElement("div");
        div.className="root";
        div.classList.add(`root${id}`);

        const img=document.createElement("button");
        img.className="btn";
        img.innerHTML=`<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 50 50" width="50px" height="50px"><path d="M 21 2 C 19.354545 2 18 3.3545455 18 5 L 18 7 L 10.154297 7 A 1.0001 1.0001 0 0 0 9.984375 6.9863281 A 1.0001 1.0001 0 0 0 9.8398438 7 L 8 7 A 1.0001 1.0001 0 1 0 8 9 L 9 9 L 9 45 C 9 46.645455 10.354545 48 12 48 L 38 48 C 39.645455 48 41 46.645455 41 45 L 41 9 L 42 9 A 1.0001 1.0001 0 1 0 42 7 L 40.167969 7 A 1.0001 1.0001 0 0 0 39.841797 7 L 32 7 L 32 5 C 32 3.3545455 30.645455 2 29 2 L 21 2 z M 21 4 L 29 4 C 29.554545 4 30 4.4454545 30 5 L 30 7 L 20 7 L 20 5 C 20 4.4454545 20.445455 4 21 4 z M 11 9 L 18.832031 9 A 1.0001 1.0001 0 0 0 19.158203 9 L 30.832031 9 A 1.0001 1.0001 0 0 0 31.158203 9 L 39 9 L 39 45 C 39 45.554545 38.554545 46 38 46 L 12 46 C 11.445455 46 11 45.554545 11 45 L 11 9 z M 18.984375 13.986328 A 1.0001 1.0001 0 0 0 18 15 L 18 40 A 1.0001 1.0001 0 1 0 20 40 L 20 15 A 1.0001 1.0001 0 0 0 18.984375 13.986328 z M 24.984375 13.986328 A 1.0001 1.0001 0 0 0 24 15 L 24 40 A 1.0001 1.0001 0 1 0 26 40 L 26 15 A 1.0001 1.0001 0 0 0 24.984375 13.986328 z M 30.984375 13.986328 A 1.0001 1.0001 0 0 0 30 15 L 30 40 A 1.0001 1.0001 0 1 0 32 40 L 32 15 A 1.0001 1.0001 0 0 0 30.984375 13.986328 z"/></svg>`;

        img.addEventListener("click",e=>{
            const deletePost=this.request;
            deletePost.deletePost(id).then(data=>{
                if(data===200){
                    div.remove();
                };
            });
        })

        const titleName=document.createElement("h2");
        titleName.className="titleName";
        titleName.innerHTML=name;

        const postEmail=document.createElement("p");
        postEmail.className="postEmail";
        postEmail.innerHTML=email;

        const titleText=document.createElement("h3");
        titleText.className="titleText";
        titleText.innerHTML=title;
        
        const text=document.createElement("p");
        text.className="text";
        text.innerHTML=body;

        div.append(titleName,postEmail,img,titleText,text);
        document.body.append(div);
    }
}

const newCard=new Card();
newCard.getPosts();